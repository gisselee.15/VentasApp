htroot.createSuggestOpenFormButton = function(msId, formId, inputId, formUrl,
		formWidth, formHeight) {

	var saveListener = function(window, fid, response) {
		if (response.success) {
			htroot.setMagicSuggestValue(formId, inputId, response.data);
			window.close();
		}
	};

	$('#' + formId + inputId.replace('.', '\\.') + 'showForm').click(
			function() {
				htroot.openFormInWindow(formId, formUrl, formWidth, formHeight,
						saveListener);
			});
}
