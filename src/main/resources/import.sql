
INSERT INTO userinfo(id, username, name, surname, active, email, password, male) VALUES (1, 'root', 'Super', 'Usuario', True, 'root@gmail.com', MD5('sql'), TRUE);

INSERT INTO permission(id, code, description) VALUES (1, 'root','Permiso para ser Super Usuario');
INSERT INTO permission(id, code, description) VALUES(3, 'Pais_sel','Permiso para ver paises');
INSERT INTO role(id, code, description) VALUES(1, 'root','Rol SuperUsuario');
INSERT INTO rolepermission(id, permission_id, role_id) VALUES(1, 1, 1);
INSERT INTO userrole(id, role_id, user_id) VALUES(1, 1, 1);

INSERT INTO config(id, defaultdatasource) VALUES(nextval('config_id_seq'), 'java:jboss/datasources/ventappDS');

CREATE TABLE public.auditreventity
(
  id integer NOT NULL,
  "timestamp" bigint NOT NULL,
  created timestamp without time zone,
  screenname VARCHAR(50),
  username VARCHAR(18),
  CONSTRAINT auditreventity_pkey PRIMARY KEY (id)
)


INSERT INTO permission(id, code, description) 
VALUES (100, 'TipoCliente_sel','Permiso para ver tipos de clientes');
INSERT INTO permission(id, code, description) 
VALUES (101, 'TipoCliente_ins','Permiso para agregar tipos de clientes');
INSERT INTO permission(id, code, description) 
VALUES (102, 'TipoCliente_upd','Permiso para actualizar tipos de clientes');
INSERT INTO permission(id, code, description) 
VALUES (103, 'TipoCliente_del','Permiso para borrar tipos de clientes');
