package py.com.owl.ventapp.cliente.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.cliente.bc.ClienteBC;
import py.com.owl.ventapp.cliente.dao.ClienteDao;
import py.com.owl.ventapp.cliente.domain.Cliente;

@Component
@Scope("session")
public class ClienteBCImpl extends BaseBCImpl<Cliente> implements ClienteBC {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private ClienteDao clienteDao;

	@Override
	public ClienteDao getDAOInstance() {

		return clienteDao;
	}

}
