package py.com.owl.ventapp.cliente.domain;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.TipoCliente;
import py.una.cnc.htroot.core.domain.EntityWithImage;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "cliente_codigo_uk", columnNames = { "empresa_id", "codigo" }),
		@UniqueConstraint(name = "cliente_ruc_uk", columnNames = { "empresa_id", "ruc" }) })

@JsonIgnoreProperties("multipartFile")
public class Cliente extends GenericEntity implements EntityWithImage {
	private static final String SECUENCIA = "cliente_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{cliente.codigo.notNull}")
	@NotBlank(message = "{cliente.codigo.notBlank}")
	@Size(max = 18, message = "{cliente.codigo.size}")
	private String codigo;

	@Size(max = 18, message = "{cliente.ruc.size}")
	private String ruc;

	@NotNull(message = "{cliente.nombre.notNull}")
	@NotBlank(message = "{cliente.nombre.notBlank}")
	@Size(max = 100, message = "{cliente.nombre.size}")
	private String nombre;

	@NotNull(message = "{cliente.apellido.notNull}")
	@NotBlank(message = "{cliente.apellido.notBlank}")
	@Size(max = 100, message = "{cliente.apellido.size}")
	private String apellido;

	@Email(message = "{email.invalido}")
	@Size(max = 100, message = "{cliente.email.size}")
	private String email;

	@JsonFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "cliente_tipocliente_fk"))
	@NotNull(message = "{cliente.tipoCliente.notNull}")
	private TipoCliente tipoCliente;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "cliente_empresa_fk"))
	@NotNull(message = "{cliente.empresa.notNull}")
	private Empresa empresa;

	@JsonIgnore
	@Lob
	@Basic(fetch = FetchType.LAZY, optional = true)
	private byte[] image;// logo

	private transient MultipartFile multipartFile;

	public Cliente() {
		// pasar validación @NotNull
		empresa = new Empresa();

	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public byte[] getImage() {
		return image;
	}

	@Override
	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	@Override
	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public String getStr() {
		return codigo + " - " + nombre + " " + apellido;
	}

}
