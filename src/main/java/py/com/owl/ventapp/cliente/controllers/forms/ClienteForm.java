package py.com.owl.ventapp.cliente.controllers.forms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.cliente.bc.ClienteBC;
import py.com.owl.ventapp.cliente.domain.Cliente;
import py.com.owl.ventapp.cliente.list.ClienteList;
import py.com.owl.ventapp.domain.TipoCliente;

@Controller
@SessionScope
@RequestMapping("/cliente")
public class ClienteForm extends BaseForm<Cliente> {
	@Autowired
	private ClienteBC clienteBC;
	@Autowired
	private ClienteList clienteList;
	@Autowired
	private TipoClienteBC tipoClienteBC;

	@Override
	public ClienteBC getBusinessController() {

		return clienteBC;
	}

	@Override
	protected ClienteList getListController() {
		return clienteList;
	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/cliente/cliente_index";
	}
	
	@ModelAttribute("tipoClienteList")
	public List<TipoCliente> x() {
		return tipoClienteBC.findEntities();
	}
}
