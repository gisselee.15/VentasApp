package py.com.owl.ventapp.cliente.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.cliente.dao.ClienteDao;
import py.com.owl.ventapp.cliente.domain.Cliente;

//@Component
@Repository
@Scope("session")
public class ClienteDaoImpl extends BaseDaoImpl<Cliente> implements ClienteDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
