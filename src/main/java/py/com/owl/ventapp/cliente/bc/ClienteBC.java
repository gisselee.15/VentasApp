package py.com.owl.ventapp.cliente.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.cliente.domain.Cliente;

public interface ClienteBC extends BaseBC<Cliente> {

}
