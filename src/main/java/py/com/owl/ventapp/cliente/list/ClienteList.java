package py.com.owl.ventapp.cliente.list;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.cliente.bc.ClienteBC;
import py.com.owl.ventapp.cliente.domain.Cliente;
import py.una.cnc.htroot.report.ReportColumn;

@Controller
@SessionScope
@RequestMapping("/cliente")
public class ClienteList extends BaseList<Cliente> {
	@Autowired
	private ClienteBC clienteBC;//

	@Override
	public ClienteBC getBusinessController() {

		return clienteBC;
	}

	@Override
	protected String getFilterableColumns() {

		return "codigo||nombre||apellido||tipoCliente.nombre";
	}

	@Override
	public String[] getTableColumns() {

		return new String[] { "id", "codigo", "nombre", "apellido", "tipoCliente.nombre" };
	}

	@Override
	public List<ReportColumn> getReportColumnList() {
		List<ReportColumn> columnas = new ArrayList<>();
		columnas.add(new ReportColumn("rownum", 30));
		ReportColumn cod = new ReportColumn("codigo", 70, HorizontalAlign.CENTER);
		columnas.add(cod);
		columnas.add(new ReportColumn("nombre", 100, HorizontalAlign.RIGHT));
		columnas.add(new ReportColumn("apellido", 100));
		columnas.add(new ReportColumn("email", 100));
		columnas.add(new ReportColumn("tipoCliente.nombre", 100));
		columnas.add(new ReportColumn("fechaNacimiento", 100, "dd/MM/yyyy"));
		return columnas;
	}

	@Override
	protected Page getPage() {
		return Page.Page_A4_Landscape();
	}

}
