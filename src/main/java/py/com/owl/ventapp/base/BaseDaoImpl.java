package py.com.owl.ventapp.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import py.com.owl.ventapp.main.AppSession;
import py.com.owl.ventapp.util.Log;
import py.una.cnc.htroot.core.dao.impl.DaoImpl;

public abstract class BaseDaoImpl<T extends GenericEntity> extends DaoImpl<T> {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	protected AppSession appSession;

	protected Log log = new Log(getClass());

	@Override
	@Transactional
	public void create(T object) {
		object.setEmpresa(appSession.getEmpresa());
		super.create(object);
	}

	@Override
	@Transactional
	public void edit(T object) {
		object.setEmpresa(appSession.getEmpresa());
		super.edit(object);
	}

	@Override
	@Transactional
	public List<T> findEntities() {

		return findEntitiesByCondition("WHERE empresa_id = ?1", appSession.getEmpresa().getId());
	}

}
