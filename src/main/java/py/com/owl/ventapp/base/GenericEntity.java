package py.com.owl.ventapp.base;

import py.com.owl.ventapp.domain.Empresa;
import py.una.cnc.htroot.core.domain.Model;

public abstract class GenericEntity extends Model {

	@Override
	public abstract Long getId();

	public abstract void setEmpresa(Empresa empresa);

}
