package py.com.owl.ventapp.base;

import java.util.List;
import java.util.Map;

import py.una.cnc.htroot.controllers.ListController;
import py.una.cnc.lib.db.dataprovider.DataTableModel;

public abstract class BaseList<T extends GenericEntity> extends ListController<T> {

	@Override
	protected DataTableModel<?> getDataTable(Integer iDisplayStart, Integer iDisplayLength, String orderBy,
			String sSearch, Map<String, String> params) {
		DataTableModel<T> dt = new DataTableModel<>();
		List<T> list = getBusinessController().findEntities();
		dt.setAaData(list);
		Long size = Long.valueOf(list.size());
		dt.setRecordsTotal(size);
		dt.setRecordsFiltered(size);
		return dt;
	}
}
