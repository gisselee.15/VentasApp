package py.com.owl.ventapp.venta.bc.impl;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.bc.ProductoBC;
import py.com.owl.ventapp.producto.bc.SubproductoBC;
import py.com.owl.ventapp.producto.dao.ProductoDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.Subproducto;
import py.una.cnc.htroot.core.dao.Dao;

@Component
@SessionScope
public class VentaCabBCImpl extends BaseBCImpl<Producto> implements ProductoBC {

	private static final long serialVersionUID = 1L;
	@Autowired
	private ProductoDao productoDao;
	@Autowired
	private SubproductoBC subProductoBC;

	@Override
	public Dao<Producto> getDAOInstance() {
		return productoDao;
	}

	@Override
	@Transactional
	public void create(Producto producto) {
		super.create(producto);
		if (BooleanUtils.isFalse(producto.getCtrlSubproducto())) {
			// debo generar subproducto
			crearSub(producto);
		}
	}

	@Override
	@Transactional
	public void edit(Producto producto) {
		super.edit(producto);
		/* Se debe generar subproducto, pero solo si ya no existe */
		if (BooleanUtils.isFalse(producto.getCtrlSubproducto())) {

			Subproducto sub = subProductoBC.buscarGeneradoPorProducto(producto);
			if (sub == null) {
				crearSub(producto);
			} else {
				sub.setPrecio(producto.getPrecio());
				subProductoBC.edit(sub);
			}
		}
	}

	private void crearSub(Producto producto) {
		Subproducto sp = new Subproducto();
		sp.setProducto(producto);
		sp.setPrecio(producto.getPrecio());
		sp.setGenerado(Boolean.TRUE);
		subProductoBC.create(sp);
	}
}
