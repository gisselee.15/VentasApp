package py.com.owl.ventapp.venta.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.Subproducto;

public interface SubproductoBC extends BaseBC<Subproducto> {
	Subproducto buscarGeneradoPorProducto(Producto producto);
}
