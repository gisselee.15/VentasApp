package py.com.owl.ventapp.venta.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Producto;

public interface VentaCabBC extends BaseBC<Producto> {

}
