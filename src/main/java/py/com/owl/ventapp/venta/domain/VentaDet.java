package py.com.owl.ventapp.venta.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.producto.domain.Subproducto;

@Entity
@Audited
public class VentaDet extends GenericEntity {
	private static final String SECUENCIA = "ventadet_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventadet_ventacab_fk"))
	@NotNull(message = "{ventaDet.ventaCab.notNull}")
	private VentaCab ventaCab;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventadet_subproducto_fk"))
	@NotNull(message = "{ventaDet.subproducto.notNull}")
	private Subproducto subproducto;
	/**
	 * Mínimo es cero porque se puede ajustar precio sin devolver producto
	 */
	@NotNull(message = "{ventaDet.cantidad.notNull}")
	@Min(value = 0, message = "{ventaDet.cantidad.min}")
	private Float cantidad;

	@NotNull(message = "{ventaDet.precioVenta.notNull}")
	@Min(value = 0, message = "{ventaDet.precioVenta.min}")
	private BigDecimal precioVenta;// precioUnitario
	/* cantidad * precioVenta */
	@NotNull(message = "{ventaDet.precioTotal.notNull}")
	@Min(value = 0, message = "{ventaDet.precioTotal.min}")
	private BigDecimal precioTotal;

	@Min(value = 0, message = "{ventaDet.porcIva.min}")
	@NotNull(message = "{ventaDet.porcIva.notNull}")
	private Float porcIva;

	@Min(value = 0, message = "{ventaDet.montoIva.min}")
	@NotNull(message = "{ventaDet.montoIva.notNull}")
	private BigDecimal montoIva;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventadet_empresa_fk"))
	@NotNull(message = "{ventaDet.empresa.notNull}")
	private Empresa empresa;

	public VentaDet() {
		// pasar validación @NotNull
		empresa = new Empresa();
		ventaCab = new VentaCab();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public VentaCab getVentaCab() {
		return ventaCab;
	}

	public void setVentaCab(VentaCab ventaCab) {
		this.ventaCab = ventaCab;
	}

	public Subproducto getSubproducto() {
		return subproducto;
	}

	public void setSubproducto(Subproducto subproducto) {
		this.subproducto = subproducto;
	}

	public Float getCantidad() {
		return cantidad;
	}

	public void setCantidad(Float cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public BigDecimal getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}

	public Float getPorcIva() {
		return porcIva;
	}

	public void setPorcIva(Float porcIva) {
		this.porcIva = porcIva;
	}

	public BigDecimal getMontoIva() {
		return montoIva;
	}

	public void setMontoIva(BigDecimal montoIva) {
		this.montoIva = montoIva;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
