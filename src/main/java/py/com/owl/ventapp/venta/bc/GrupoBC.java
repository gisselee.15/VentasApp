package py.com.owl.ventapp.venta.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Grupo;

public interface GrupoBC extends BaseBC<Grupo> {

}
