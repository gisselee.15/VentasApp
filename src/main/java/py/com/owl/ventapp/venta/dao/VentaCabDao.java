package py.com.owl.ventapp.venta.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.producto.domain.Producto;

public interface VentaCabDao extends BaseDao<Producto> {

}
