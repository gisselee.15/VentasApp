package py.com.owl.ventapp.venta.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.cliente.domain.Cliente;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;
import py.una.cnc.htroot.core.util.PersistResponse;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "ventacab_timbrado_numdoc_uk", columnNames = { "empresa_id",
		"timbrado_id", "numdoc" }) })

public class VentaCab extends GenericEntity {
	private static final String SECUENCIA = "ventacab_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventacab_timbrado_fk") )
	@NotNull(message = "{ventaCab.timbrado.notNull}")
	private Timbrado timbrado;

	@NotNull(message = "{ventaCab.numdoc.notNull}")
	private Integer numdoc;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaVenta;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventacab_cliente_fk") )
	@NotNull(message = "{ventaCab.cliente.notNull}")
	private Cliente cliente;
	// 4251235;JUAN PEREZ RODRIGUEZ
	private String clienteStr;
	// numeroTimbrado;sucursal;ptoexp;numdoc: 22544;001;003;120
	private String docInfo;
	@NotNull(message = "{ventaCab.contado.notNull}")
	private Boolean contado;
	@Min(value = 1, message = "{ventaCab.cantCuotas.min}")
	private Integer cantCuotas;
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "ventacab_sucursal_fk") )
	@NotNull(message = "{ventaCab.sucursal.notNull}")
	private Sucursal sucursal;

	private BigDecimal montoTotal = BigDecimal.ZERO;
	private Integer anho;
	private Integer mes;
	private Integer dia;

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	private transient List<VentaDet> ventaDetList;
	private transient List<PersistResponse<VentaDet>> ventaDetListResponse;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "cliente_empresa_fk") )
	@NotNull(message = "{cliente.empresa.notNull}")
	private Empresa empresa;

	public VentaCab() {
		// pasar validación @NotNull
		empresa = new Empresa();
		fechaVenta = new Date();
		ventaDetList = new ArrayList<>();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Timbrado getTimbrado() {
		return timbrado;
	}

	public void setTimbrado(Timbrado timbrado) {
		this.timbrado = timbrado;
	}

	public Integer getNumdoc() {
		return numdoc;
	}

	public void setNumdoc(Integer numdoc) {
		this.numdoc = numdoc;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getClienteStr() {
		return clienteStr;
	}

	public void setClienteStr(String clienteStr) {
		this.clienteStr = clienteStr;
	}

	public String getDocInfo() {
		return docInfo;
	}

	public void setDocInfo(String docInfo) {
		this.docInfo = docInfo;
	}

	public Boolean getContado() {
		return contado;
	}

	public void setContado(Boolean contado) {
		this.contado = contado;
	}

	public Integer getCantCuotas() {
		return cantCuotas;
	}

	public void setCantCuotas(Integer cantCuotas) {
		this.cantCuotas = cantCuotas;
	}

	public Integer getAnho() {
		return anho;
	}

	public void setAnho(Integer anho) {
		this.anho = anho;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public List<VentaDet> getVentaDetList() {
		return ventaDetList;
	}

	public void setVentaDetList(List<VentaDet> ventaDetList) {
		this.ventaDetList = ventaDetList;
	}

	public List<PersistResponse<VentaDet>> getVentaDetListResponse() {
		return ventaDetListResponse;
	}

	public void setVentaDetListResponse(List<PersistResponse<VentaDet>> ventaDetListResponse) {
		this.ventaDetListResponse = ventaDetListResponse;
	}

	@Override
	public String toString() {
		return "VentaCab [id=" + id + ", numdoc=" + numdoc + ", fechaVenta=" + fechaVenta + ", cliente=" + cliente
				+ "]";
	}

}
