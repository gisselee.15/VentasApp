package py.com.owl.ventapp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {

	private final Logger logger;

	public Log(Class<?> clazz) {

		logger = LoggerFactory.getLogger(clazz);
	}

	public void info(String msg, Object... args) {

		logger.info(msg, args);
	}

	public void error(String msg, Throwable th) {
		logger.error(msg, th);
	}

}
