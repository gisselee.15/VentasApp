package py.com.owl.ventapp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.springframework.stereotype.Component;
import py.una.cnc.htroot.core.main.ApplicationContextProvider;
import py.una.cnc.htroot.core.main.Message;
import py.una.cnc.lib.core.util.AppLogger;

@Component
public class FormateadorDate extends py.una.cnc.htroot.core.util.DateFormatter {

	private AppLogger logger = new AppLogger(getClass());

	private SimpleDateFormat simpleDateFormat;

	private SimpleDateFormat simpleDateTimeFormat;
	private SimpleDateFormat simpleTimeFormat;

	public FormateadorDate() {
		super();
	}

	@Override
	public Date parse(final String text, final Locale locale) throws ParseException {

		SimpleDateFormat dateFormat = getSimpleDateTimeFormat();
		Date parsed = null;
		try {
			parsed = dateFormat.parse(text);
		} catch (Exception ex) {

			try {
				dateFormat = getSimpleDateFormat();
				parsed = dateFormat.parse(text);
			} catch (Exception ex2) {

				try {

					dateFormat = getSimpleTimeFormat();
					parsed = dateFormat.parse(text);
				} catch (Exception ex3) {
					logger.error("No se pudo formatear fecha {} -> {}, {}", text, ex3.getMessage(),
							dateFormat.toPattern());
				}
			}
		}

		return parsed;
	}

	@Override
	public String print(final Date object, final Locale locale) {

		final SimpleDateFormat dateFormat = getSimpleDateTimeFormat();
		return dateFormat.format(object);
	}

	private SimpleDateFormat getSimpleDateFormat() {

		if (simpleDateFormat == null) {
			Message msg = (Message) ApplicationContextProvider.getBeanStatic("message");
			String dateFormat = msg.getOrDefault("date.format", "dd/MM/yyyy");
			simpleDateFormat = new SimpleDateFormat(dateFormat);
		}
		return simpleDateFormat;
	}

	private SimpleDateFormat getSimpleDateTimeFormat() {

		if (simpleDateTimeFormat == null) {
			Message msg = (Message) ApplicationContextProvider.getBeanStatic("message");
			String dateTimeFormat = msg.getOrDefault("datetime.format", "dd/MM/yyyy HH:mm");
			simpleDateTimeFormat = new SimpleDateFormat(dateTimeFormat);
		}
		return simpleDateTimeFormat;
	}

	private SimpleDateFormat getSimpleTimeFormat() {

		if (simpleTimeFormat == null) {
			Message msg = (Message) ApplicationContextProvider.getBeanStatic("message");
			String dateTimeFormat = msg.getOrDefault("time.format", "HH:mm");
			simpleTimeFormat = new SimpleDateFormat(dateTimeFormat);
		}
		return simpleTimeFormat;
	}
}
