package py.com.owl.ventapp.converters;

import org.springframework.stereotype.Component;
import py.com.owl.ventapp.dao.SucursalDao;
import py.com.owl.ventapp.domain.Sucursal;
import py.una.cnc.htroot.converters.ModelConverter;
import py.una.cnc.htroot.core.main.ApplicationContextProvider;

@Component
public class SucursalConverter extends ModelConverter<Sucursal> {

	@Override
	protected SucursalDao getDaoImpl() {

		return (SucursalDao) ApplicationContextProvider.getBeanStatic("sucursalDaoImpl");
	}
}
