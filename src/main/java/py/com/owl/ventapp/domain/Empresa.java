package py.com.owl.ventapp.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import py.com.owl.ventapp.base.GenericEntity;
import py.una.cnc.htroot.core.domain.EntityWithImage;
import py.una.cnc.htroot.core.main.ApplicationContextProvider;
import py.una.cnc.htroot.core.util.PersistResponse;
import py.una.cnc.htroot.core.util.PersistResponseUtil;

/**
 * @author Diego Cerrano
 * @version 1.0.0 20/05/2017
 *
 */

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "empresa_codigo_uk", columnNames = { "codigo" }),
		@UniqueConstraint(name = "empresa_ruc_uk", columnNames = { "ruc" }),
		@UniqueConstraint(name = "empresa_razonSocial_uk", columnNames = { "razonSocial" }) })
@JsonIgnoreProperties("multipartFile")
public class Empresa extends GenericEntity implements EntityWithImage {

	private static final String SECUENCIA = "empresa_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{empresa.codigo.notNull}")
	@NotBlank(message = "{empresa.codigo.notBlank}")
	@Size(max = 10, message = "{empresa.codigo.size}")
	private String codigo;

	@NotNull(message = "{empresa.ruc.notNull}")
	@NotBlank(message = "{empresa.ruc.notBlank}")
	@Size(max = 18, message = "{empresa.ruc.size}")
	private String ruc;

	@NotNull(message = "{empresa.razonSocial.notNull}")
	@NotBlank(message = "{empresa.razonSocial.notBlank}")
	@Size(max = 100, message = "{empresa.razonSocial.size}")
	private String razonSocial;
	// MJT
	@Size(max = 18, message = "{empresa.nroPatronal.size}")
	private String nroPatronal = "00";

	@Size(max = 18, message = "{empresa.nroPatronalIps.size}")
	private String nroPatronalIps = "00";

	@Email(message = "{email.invalido}")
	@Size(max = 100, message = "{empresa.email.size}")
	private String email;

	@JsonIgnore
	@Lob
	@Basic(fetch = FetchType.LAZY, optional = true)
	private byte[] image;// logo

	private boolean activo = true;

	private transient MultipartFile multipartFile;
	private transient List<Sucursal> sucursales = new ArrayList<>();
	private transient List<PersistResponse<Sucursal>> sucursalPRList;

	public Empresa() {

	}

	public Empresa(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public void setId(Long id) {

		this.id = id;
	}

	public String getCodigo() {

		return codigo;
	}

	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getRuc() {

		return ruc;
	}

	public void setRuc(String ruc) {

		this.ruc = ruc;
	}

	public String getRazonSocial() {

		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {

		this.razonSocial = razonSocial;
	}

	public boolean isActivo() {

		return activo;
	}

	public void setActivo(boolean activo) {

		this.activo = activo;
	}

	@Override
	public byte[] getImage() {

		return image;
	}

	@Override
	public void setImage(byte[] image) {

		this.image = image;
	}

	public String getNroPatronal() {

		return nroPatronal;
	}

	public void setNroPatronal(String nroPatronal) {

		this.nroPatronal = nroPatronal;
	}

	public String getNroPatronalIps() {

		return nroPatronalIps;
	}

	public void setNroPatronalIps(String nroPatronalIps) {

		this.nroPatronalIps = nroPatronalIps;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public List<Sucursal> getSucursales() {

		return sucursales;
	}

	public void setSucursales(List<Sucursal> sucursales) {

		this.sucursales = sucursales;
	}

	public List<PersistResponse<Sucursal>> getSucursalPRList() {

		if (sucursalPRList == null) {
			PersistResponseUtil persistR = (PersistResponseUtil) ApplicationContextProvider
					.getBeanStatic("persistResponseUtil");
			sucursalPRList = persistR.getPersistResponseList(Sucursal.class, sucursales, true);
		}
		return sucursalPRList;
	}

	public void setSucursalPRList(List<PersistResponse<Sucursal>> sucursalPRList) {

		this.sucursalPRList = sucursalPRList;
	}

	@Override
	public void setMultipartFile(MultipartFile multipartFile) {

		this.multipartFile = multipartFile;
	}

	@Override
	public MultipartFile getMultipartFile() {

		return multipartFile;
	}

	@Override
	public String toString() {

		return "Empresa [codigo=" + codigo + ", ruc=" + ruc + ", razonSocial=" + razonSocial + "]";
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		// TODO Auto-generated method stub

	}

}
