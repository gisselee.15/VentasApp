package py.com.owl.ventapp.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import py.com.owl.ventapp.base.GenericEntity;

@Entity
@Audited
@Table(uniqueConstraints = {
		@UniqueConstraint(name = "tipoProveedor_codigo_uk", columnNames = { "empresa_id", "codigo" }),
		@UniqueConstraint(name = "tipoProveedor_nombre_uk", columnNames = { "empresa_id", "nombre" }) })
public class TipoProveedor extends GenericEntity {

	private static final String SECUENCIA = "tipoProveedor_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{tipoProveedor.codigo.notNull}")
	@NotBlank(message = "{tipoProveedor.codigo.notBlank}")
	@Size(max = 10, message = "{tipoProveedor.codigo.size}")
	private String codigo;

	@NotNull(message = "{tipoProveedor.nombre.notNull}")
	@NotBlank(message = "{tipoProveedor.nombre.notBlank}")
	@Size(max = 100, message = "{tipoProveedor.nombre.size}")
	private String nombre;

	@ManyToOne
	@NotNull(message = "{tipoProveedor.empresa.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "tipoProveedor_empresa_fk"))
	private Empresa empresa;

	public TipoProveedor() {
		this.empresa = new Empresa();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;

	}

}
