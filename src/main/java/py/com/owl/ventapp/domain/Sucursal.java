package py.com.owl.ventapp.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;

/**
 * @author Diego Cerrano
 * @version 1.0.0 20/05/2017
 *
 */
@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "sucursal_codigo_uk", columnNames = { "empresa_id", "codigo" }),
		@UniqueConstraint(name = "sucursal_nombre_uk", columnNames = { "empresa_id", "nombre" }) })
public class Sucursal extends GenericEntity {

	private static final String SECUENCIA = "sucursal_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{sucursal.codigo.notNull}")
	@NotBlank(message = "{sucursal.codigo.notBlank}")
	@Size(max = 10, message = "{sucursal.codigo.size}")
	private String codigo;

	@NotNull(message = "{sucursal.nombre.notNull}")
	@NotBlank(message = "{sucursal.nombre.notBlank}")
	@Size(max = 100, message = "{sucursal.nombre.size}")
	private String nombre;

	@Size(max = 100, message = "{sucursal.direccion.size}")
	private String direccion;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "sucursal_empresa_fk"))
	@NotNull(message = "{sucursal.empresa.notNull}")
	private Empresa empresa;

	private boolean activo = true;

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public void setId(Long id) {

		this.id = id;
	}

	public String getCodigo() {

		return codigo;
	}

	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getNombre() {

		return nombre;
	}

	public void setNombre(String nombre) {

		this.nombre = nombre;
	}

	public boolean isActivo() {

		return activo;
	}

	public void setActivo(boolean activo) {

		this.activo = activo;
	}

	public String getDireccion() {

		return direccion;
	}

	public void setDireccion(String direccion) {

		this.direccion = direccion;
	}

	public Empresa getEmpresa() {

		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	@Override
	public String toString() {

		return "Sucursal [codigo=" + codigo + ", nombre=" + nombre + ", empresa=" + empresa + "]";
	}

}
