package py.com.owl.ventapp.producto.controller.form;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.producto.bc.FamiliaBC;
import py.com.owl.ventapp.producto.bc.GrupoBC;
import py.com.owl.ventapp.producto.bc.IvaBC;
import py.com.owl.ventapp.producto.bc.LineaBC;
import py.com.owl.ventapp.producto.bc.ProductoBC;
import py.com.owl.ventapp.producto.controller.list.ProductoList;
import py.com.owl.ventapp.producto.domain.Familia;
import py.com.owl.ventapp.producto.domain.Producto;
import py.una.cnc.htroot.controllers.ListController;

@Controller
@SessionScope
@RequestMapping("/producto")
public class ProductoForm extends BaseForm<Producto> {
	@Autowired
	private ProductoBC productoBC;
	@Autowired
	private ProductoList productoList;

	@Autowired
	private FamiliaBC familiaBC;
	@Autowired
	private LineaBC lineaBC;
	@Autowired
	private GrupoBC grupoBC;
	@Autowired
	private IvaBC ivaBC;

	@Override
	public ProductoBC getBusinessController() {

		return productoBC;
	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "abm/producto/producto_index";
	}

	@Override
	protected ListController<Producto> getListController() {

		return productoList;
	}

	@Override
	protected void addExtraAttributes(Producto bean, ModelMap modelMap) {

		modelMap.addAttribute("familiaList", familiaBC.findEntities());
		modelMap.addAttribute("lineaList", lineaBC.findEntities());
		modelMap.addAttribute("grupoList", grupoBC.findEntities());
		modelMap.addAttribute("ivaList", ivaBC.findEntities());

		super.addExtraAttributes(bean, modelMap);
	}

	@ModelAttribute("fList")
	public List<Familia> getFamList() {
		return familiaBC.findEntities();
	}

}
