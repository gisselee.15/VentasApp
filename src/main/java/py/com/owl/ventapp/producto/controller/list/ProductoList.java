package py.com.owl.ventapp.producto.controller.list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.producto.bc.ProductoBC;
import py.com.owl.ventapp.producto.domain.Producto;

@Controller
@SessionScope
@RequestMapping("/producto")
public class ProductoList extends BaseList<Producto> {
	@Autowired
	private ProductoBC productoBC;

	@Override
	public ProductoBC getBusinessController() {

		return productoBC;
	}

	@Override
	public String[] getTableColumns() {
		return new String[] { "id", "codigo", "nombre", "familia.nombre", "precio" };
	}

}
