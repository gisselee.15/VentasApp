package py.com.owl.ventapp.producto.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.producto.dao.SubproductoDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.Subproducto;

@Repository
@SessionScope
public class SubproductoDaoImpl extends BaseDaoImpl<Subproducto> implements SubproductoDao {
	@Autowired
	private EntityManager em;

	@Override
	public Subproducto buscarGeneradoPorProducto(Producto producto) {
		/*
		 * SELECT * FROM subproducto WHERE producto_id = ? AND generado IS TRUE
		 */
		String jpql = "SELECT object(S) FROM Subproducto AS S" + " WHERE producto_id = ?1 AND generado IS TRUE";

		Query query = em.createQuery(jpql);
		query.setParameter(1, producto.getId());
		return (Subproducto) query.getSingleResult();
	}

}
