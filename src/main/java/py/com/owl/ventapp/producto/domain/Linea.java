package py.com.owl.ventapp.producto.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "linea_codigo_uk", columnNames = { "empresa_id", "codigo" }),
		@UniqueConstraint(name = "linea_nombre_uk", columnNames = { "empresa_id", "nombre" }) })
public class Linea extends GenericEntity {

	private static final String SECUENCIA = "linea_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{linea.codigo.notNull}")
	@NotBlank(message = "{linea.codigo.notBlank}")
	@Size(max = 10, message = "{linea.codigo.size}")
	private String codigo;

	@NotNull(message = "{linea.nombre.notNull}")
	@NotBlank(message = "{linea.nombre.notBlank}")
	@Size(max = 100, message = "{linea.nombre.size}")
	private String nombre;

	@JsonIgnore
	@ManyToOne
	@NotNull(message = "{linea.empresa.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "linea_empresa_fk"))
	private Empresa empresa;

	public Linea() {
		// para pasar validación @NotNull
		empresa = new Empresa();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;

	}

}
