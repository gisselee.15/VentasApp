package py.com.owl.ventapp.producto.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Producto;

public interface ProductoBC extends BaseBC<Producto> {

}
