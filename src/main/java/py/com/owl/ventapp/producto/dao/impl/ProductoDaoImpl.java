package py.com.owl.ventapp.producto.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.producto.dao.ProductoDao;
import py.com.owl.ventapp.producto.domain.Producto;

@Repository
@SessionScope
public class ProductoDaoImpl extends BaseDaoImpl<Producto> implements ProductoDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
