package py.com.owl.ventapp.producto.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.producto.dao.LineaDao;
import py.com.owl.ventapp.producto.domain.Linea;

@Repository
@SessionScope
public class LineaDaoImpl extends BaseDaoImpl<Linea> implements LineaDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
