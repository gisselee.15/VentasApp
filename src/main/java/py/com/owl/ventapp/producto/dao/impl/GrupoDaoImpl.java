package py.com.owl.ventapp.producto.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.producto.dao.GrupoDao;
import py.com.owl.ventapp.producto.domain.Grupo;

@Repository
@SessionScope
public class GrupoDaoImpl extends BaseDaoImpl<Grupo> implements GrupoDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
