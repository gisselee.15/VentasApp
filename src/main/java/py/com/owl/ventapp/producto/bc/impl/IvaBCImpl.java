package py.com.owl.ventapp.producto.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.bc.IvaBC;
import py.com.owl.ventapp.producto.dao.IvaDao;
import py.com.owl.ventapp.producto.domain.Iva;

@Component
@SessionScope
public class IvaBCImpl extends BaseBCImpl<Iva> implements IvaBC {

	private static final long serialVersionUID = 1L;
	@Autowired
	private IvaDao ivaDao;

	@Override
	public IvaDao getDAOInstance() {
		return ivaDao;
	}

}
