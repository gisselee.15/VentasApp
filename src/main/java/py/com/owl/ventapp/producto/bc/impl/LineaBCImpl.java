package py.com.owl.ventapp.producto.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.bc.LineaBC;
import py.com.owl.ventapp.producto.dao.LineaDao;
import py.com.owl.ventapp.producto.domain.Linea;

@Component
@SessionScope
public class LineaBCImpl extends BaseBCImpl<Linea> implements LineaBC {

	private static final long serialVersionUID = 1L;
	@Autowired
	private LineaDao lineaDao;

	@Override
	public LineaDao getDAOInstance() {
		return lineaDao;
	}

}
