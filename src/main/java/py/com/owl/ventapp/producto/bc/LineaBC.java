package py.com.owl.ventapp.producto.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Linea;

public interface LineaBC extends BaseBC<Linea> {

}
