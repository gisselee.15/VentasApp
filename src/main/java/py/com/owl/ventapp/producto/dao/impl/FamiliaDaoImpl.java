package py.com.owl.ventapp.producto.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.producto.dao.FamiliaDao;
import py.com.owl.ventapp.producto.domain.Familia;

@Repository
@SessionScope
public class FamiliaDaoImpl extends BaseDaoImpl<Familia> implements FamiliaDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
