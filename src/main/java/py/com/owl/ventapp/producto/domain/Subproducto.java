package py.com.owl.ventapp.producto.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;

@Entity
@Audited
public class Subproducto extends GenericEntity {

	private static final String SECUENCIA = "subproducto_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@ManyToOne
	@NotNull(message = "{subproducto.producto.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "subproducto_producto_fk"))
	private Producto producto;

	private Boolean generado;

	@NotNull(message = "{subproducto.precio.notNull}")
	@Min(value = 0, message = "{subproducto.precio.min}")
	private BigDecimal precio;

	@JsonIgnore
	@ManyToOne
	@NotNull(message = "{subproducto.empresa.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "subproducto_empresa_fk"))
	private Empresa empresa;

	public Subproducto() {
		// para pasar validación @NotNull
		empresa = new Empresa();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;

	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Boolean getGenerado() {
		return generado;
	}

	public void setGenerado(Boolean generado) {
		this.generado = generado;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

}
