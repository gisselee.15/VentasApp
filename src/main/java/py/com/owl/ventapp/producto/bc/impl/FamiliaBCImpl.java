package py.com.owl.ventapp.producto.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.bc.FamiliaBC;
import py.com.owl.ventapp.producto.dao.FamiliaDao;
import py.com.owl.ventapp.producto.domain.Familia;

@Component
@SessionScope
public class FamiliaBCImpl extends BaseBCImpl<Familia> implements FamiliaBC {

	private static final long serialVersionUID = 1L;
	@Autowired
	private FamiliaDao familiaDao;

	@Override
	public FamiliaDao getDAOInstance() {
		return familiaDao;
	}

}
