package py.com.owl.ventapp.producto.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.bc.GrupoBC;
import py.com.owl.ventapp.producto.dao.GrupoDao;
import py.com.owl.ventapp.producto.domain.Grupo;

@Component
@SessionScope
public class GrupoBCImpl extends BaseBCImpl<Grupo> implements GrupoBC {

	private static final long serialVersionUID = 1L;
	@Autowired
	private GrupoDao grupoDao;

	@Override
	public GrupoDao getDAOInstance() {
		return grupoDao;
	}

}
