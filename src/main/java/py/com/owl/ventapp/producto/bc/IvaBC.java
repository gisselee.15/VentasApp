package py.com.owl.ventapp.producto.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.Iva;

public interface IvaBC extends BaseBC<Iva> {

}
