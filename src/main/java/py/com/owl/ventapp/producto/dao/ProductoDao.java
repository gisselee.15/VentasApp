package py.com.owl.ventapp.producto.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.producto.domain.Producto;

public interface ProductoDao extends BaseDao<Producto> {

}
