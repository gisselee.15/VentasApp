package py.com.owl.ventapp.producto.domain;

import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;
import py.una.cnc.htroot.core.domain.EntityWithImage;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "producto_codigo_uk", columnNames = { "empresa_id", "codigo" }) })
public class Producto extends GenericEntity implements EntityWithImage {

	private static final String SECUENCIA = "producto_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{producto.codigo.notNull}")
	@NotBlank(message = "{producto.codigo.notBlank}")
	@Size(max = 10, message = "{producto.codigo.size}")
	private String codigo;

	@NotNull(message = "{producto.nombre.notNull}")
	@NotBlank(message = "{producto.nombre.notBlank}")
	@Size(max = 200, message = "{producto.nombre.size}")
	private String nombre;

	@ManyToOne
	@NotNull(message = "{producto.familia.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "producto_familia_fk"))
	private Familia familia;

	@ManyToOne
	@NotNull(message = "{producto.linea.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "producto_linea_fk"))
	private Linea linea;

	@ManyToOne
	@NotNull(message = "{producto.grupo.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "producto_grupo_fk"))
	private Grupo grupo;

	@ManyToOne
	@NotNull(message = "{producto.iva.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "producto_iva_fk"))
	private Iva iva;

	@NotNull(message = "{producto.precio.notNull}")
	@Min(value = 0, message = "{producto.precio.min}")
	private BigDecimal precio;

	private Boolean vendible;
	private Boolean precioModificable;
	private Boolean ctrlSubproducto;

	@JsonIgnore
	@ManyToOne
	@NotNull(message = "{producto.empresa.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "producto_empresa_fk"))
	private Empresa empresa;

	@JsonIgnore
	private transient MultipartFile multipartFile;

	// @JsonIgnore
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] foto;

	public Producto() {
		// para pasar validación @NotNull
		empresa = new Empresa();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;

	}

	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	public Linea getLinea() {
		return linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public Iva getIva() {
		return iva;
	}

	public void setIva(Iva iva) {
		this.iva = iva;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Boolean getVendible() {
		return vendible;
	}

	public void setVendible(Boolean vendible) {
		this.vendible = vendible;
	}

	public Boolean getPrecioModificable() {
		return precioModificable;
	}

	public void setPrecioModificable(Boolean precioModificable) {
		this.precioModificable = precioModificable;
	}

	public Boolean getCtrlSubproducto() {
		return ctrlSubproducto;
	}

	public void setCtrlSubproducto(Boolean ctrlSubproducto) {
		this.ctrlSubproducto = ctrlSubproducto;
	}

	@Override
	public byte[] getImage() {

		return foto;
	}

	@Override
	public void setImage(byte[] image) {
		this.foto = image;
	}

	@Override
	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	@Override
	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

}
