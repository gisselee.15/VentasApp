package py.com.owl.ventapp.producto.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;

/**
 * @author Diego Cerrano
 * @version 1.0.0 20/05/2017
 *
 */
@Entity
@Audited
@Table(uniqueConstraints = {
		@UniqueConstraint(name = "unidadmedida_codigo_uk", columnNames = { "empresa_id", "codigo" }),
		@UniqueConstraint(name = "unidadmedida_nombre_uk", columnNames = { "empresa_id", "nombre" }) })
public class UnidadMedida extends GenericEntity {

	private static final String SECUENCIA = "unidadmedida_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{unidadMedida.codigo.notNull}")
	@NotBlank(message = "{unidadMedida.codigo.notBlank}")
	@Size(max = 10, message = "{unidadMedida.codigo.size}")
	private String codigo;

	@NotNull(message = "{unidadMedida.nombre.notNull}")
	@NotBlank(message = "{unidadMedida.nombre.notBlank}")
	@Size(max = 100, message = "{unidadMedida.nombre.size}")
	private String nombre;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "unidadmedida_empresa_fk"))
	@NotNull(message = "{sucursal.empresa.notNull}")
	private Empresa empresa;

	public UnidadMedida() {
		// solo para pasar validación @NotNull,se obtiene de sesión
		empresa = new Empresa();
	}

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public void setId(Long id) {

		this.id = id;
	}

	public Empresa getEmpresa() {

		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	public String getCodigo() {

		return codigo;
	}

	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getNombre() {

		return nombre;
	}

	public void setNombre(String nombre) {

		this.nombre = nombre;
	}

	@Override
	public String toString() {

		return "Caja [nombre=" + nombre + "]";
	}
}
