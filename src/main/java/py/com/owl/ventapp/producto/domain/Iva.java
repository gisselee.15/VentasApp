package py.com.owl.ventapp.producto.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.owl.ventapp.base.GenericEntity;
import py.com.owl.ventapp.domain.Empresa;

@Entity
@Audited
@Table(uniqueConstraints = { @UniqueConstraint(name = "iva_nombre_uk", columnNames = { "empresa_id", "nombre" }) })
public class Iva extends GenericEntity {

	@Id
	private Long id;

	@NotNull
	@Size(max = 100)
	private String nombre;

	@NotNull
	@Min(value = 0)
	private Float porcentaje;

	@NotNull
	@Min(value = 0)
	private Float baseImponible;

	@JsonIgnore
	@ManyToOne
	@NotNull
	@JoinColumn(foreignKey = @ForeignKey(name = "iva_empresa_fk"))
	private Empresa empresa;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Float getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Float porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Float getBaseImponible() {
		return baseImponible;
	}

	public void setBaseImponible(Float baseImponible) {
		this.baseImponible = baseImponible;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
