package py.com.owl.ventapp.producto.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.producto.bc.SubproductoBC;
import py.com.owl.ventapp.producto.dao.SubproductoDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.Subproducto;

@Service
@SessionScope
public class SubproductoBCImpl extends BaseBCImpl<Subproducto> implements SubproductoBC {

	private static final long serialVersionUID = 1L;
	@Autowired
	private SubproductoDao dao;

	@Override
	public SubproductoDao getDAOInstance() {

		return dao;
	}

	@Override
	@Transactional
	public Subproducto buscarGeneradoPorProducto(Producto producto) {

		return dao.buscarGeneradoPorProducto(producto);
	}

}
