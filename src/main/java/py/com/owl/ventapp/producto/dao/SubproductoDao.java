package py.com.owl.ventapp.producto.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.producto.domain.Producto;
import py.com.owl.ventapp.producto.domain.Subproducto;

public interface SubproductoDao extends BaseDao<Subproducto> {
	Subproducto buscarGeneradoPorProducto(Producto producto);
}
