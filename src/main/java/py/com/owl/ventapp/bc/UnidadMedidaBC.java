package py.com.owl.ventapp.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.producto.domain.UnidadMedida;

public interface UnidadMedidaBC extends BaseBC<UnidadMedida> {}
