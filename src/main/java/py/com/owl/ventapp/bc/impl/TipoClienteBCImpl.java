package py.com.owl.ventapp.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.dao.TipoClienteDao;
import py.com.owl.ventapp.domain.TipoCliente;

@Component
@Scope("session") // request
// Singleton
public class TipoClienteBCImpl extends BaseBCImpl<TipoCliente> implements TipoClienteBC {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private TipoClienteDao tipoClienteDao;

	@Override
	public TipoClienteDao getDAOInstance() {

		return tipoClienteDao;
	}

}
