package py.com.owl.ventapp.bc;

import java.util.List;
import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;

public interface SucursalBC extends BaseBC<Sucursal> {

	List<Sucursal> getListByEmpresa(Empresa empresa);
}
