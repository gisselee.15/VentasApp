package py.com.owl.ventapp.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.bc.UnidadMedidaBC;
import py.com.owl.ventapp.dao.UnidadMedidaDao;
import py.com.owl.ventapp.producto.domain.UnidadMedida;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class UnidadMedidaBCImpl extends BaseBCImpl<UnidadMedida> implements UnidadMedidaBC {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UnidadMedidaDao unidadMedidaDao;

	@Override
	public UnidadMedidaDao getDAOInstance() {

		return unidadMedidaDao;
	}

}
