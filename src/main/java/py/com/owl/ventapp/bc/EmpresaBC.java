package py.com.owl.ventapp.bc;

import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.domain.Empresa;

public interface EmpresaBC extends BaseBC<Empresa> {

}
