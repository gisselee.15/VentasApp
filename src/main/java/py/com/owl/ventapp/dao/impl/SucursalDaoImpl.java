package py.com.owl.ventapp.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.SucursalDao;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SucursalDaoImpl extends BaseDaoImpl<Sucursal> implements SucursalDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private EntityManager em;

	@Transactional
	@Override
	public List<Sucursal> getListByEmpresa(Empresa empresa) {

		return findEntitiesByCondition("WHERE empresa_id = ?1", empresa.getId());
	}

	@Override
	@Transactional
	public void create(Sucursal object) {
		em.persist(object);
		log.info("Registro {} creado", object);
	}

	@Override
	@Transactional
	public void edit(Sucursal object) {
		em.merge(object);
		log.info("Registro {} actualizado", object);
	}

}
