package py.com.owl.ventapp.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.TipoClienteDao;
import py.com.owl.ventapp.domain.TipoCliente;

//@Component
@Repository
@Scope("session")
public class TipoClienteDaoImpl extends BaseDaoImpl<TipoCliente> implements TipoClienteDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
