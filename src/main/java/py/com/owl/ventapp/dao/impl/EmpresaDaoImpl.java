package py.com.owl.ventapp.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.EmpresaDao;
import py.com.owl.ventapp.domain.Empresa;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class EmpresaDaoImpl extends BaseDaoImpl<Empresa> implements EmpresaDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private EntityManager em;

	@Override
	@Transactional
	public List<Empresa> findEntities() {
		String sql = "SELECT object(E) FROM Empresa AS E";
		Query query = em.createQuery(sql);
		return query.getResultList();
	}
}
