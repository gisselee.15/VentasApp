package py.com.owl.ventapp.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.domain.TipoCliente;

public interface TipoClienteDao extends BaseDao<TipoCliente> {

}
