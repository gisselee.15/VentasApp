package py.com.owl.ventapp.dao;

import py.com.owl.ventapp.base.BaseDao;
import py.com.owl.ventapp.producto.domain.UnidadMedida;

public interface UnidadMedidaDao extends BaseDao<UnidadMedida> {

}
