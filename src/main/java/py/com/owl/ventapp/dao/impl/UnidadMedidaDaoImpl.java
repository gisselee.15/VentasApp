package py.com.owl.ventapp.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.UnidadMedidaDao;
import py.com.owl.ventapp.producto.domain.UnidadMedida;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class UnidadMedidaDaoImpl extends BaseDaoImpl<UnidadMedida> implements UnidadMedidaDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
