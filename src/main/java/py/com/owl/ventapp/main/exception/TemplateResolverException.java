package py.com.owl.ventapp.main.exception;

public class TemplateResolverException extends IllegalArgumentException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public TemplateResolverException(String msg) {
		super(msg);
	}
}
