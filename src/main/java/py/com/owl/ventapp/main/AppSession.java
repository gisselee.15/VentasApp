package py.com.owl.ventapp.main;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.com.owl.ventapp.domain.Empresa;

@Component
@Scope("session")
public class AppSession {

	public Empresa getEmpresa() {
		// TODO: asociar empresa a usuario
		Empresa emp = new Empresa();
		emp.setId(1L);
		return emp;
	}
}
