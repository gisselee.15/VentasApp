package py.com.owl.ventapp.controllers.lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.bc.UnidadMedidaBC;
import py.com.owl.ventapp.producto.domain.UnidadMedida;

@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@RequestMapping(value = "/unidad-medida")
public class UnidadMedidaList extends BaseList<UnidadMedida> {

	@Autowired
	private UnidadMedidaBC unidadMedidaBC;

	@Override
	public UnidadMedidaBC getBusinessController() {

		return unidadMedidaBC;
	}

	@Override
	public String[] getTableColumns() {

		return new String[] { "id", "codigo", "nombre" };
	}

	@Override
	public String[] getReportColumns() {
		return new String[] { "rownum", "codigo", "nombre" };
	}

}
