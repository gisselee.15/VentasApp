package py.com.owl.ventapp.controllers.lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.bc.EmpresaBC;
import py.com.owl.ventapp.domain.Empresa;

@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@RequestMapping(value = "/empresa")
public class EmpresaList extends BaseList<Empresa> {

	@Autowired
	private EmpresaBC empresaBC;

	@Override
	public EmpresaBC getBusinessController() {

		return empresaBC;
	}

	@Override
	public String[] getTableColumns() {

		return new String[] { "id", "codigo", "ruc", "razonSocial" };
	}

}
