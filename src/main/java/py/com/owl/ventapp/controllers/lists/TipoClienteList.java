package py.com.owl.ventapp.controllers.lists;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.domain.TipoCliente;

@Controller
@Scope("session")
@RequestMapping("/tipo-cliente")
public class TipoClienteList extends BaseList<TipoCliente> {
	@Autowired
	private TipoClienteBC tipoClienteBC;//

	@Override
	public TipoClienteBC getBusinessController() {

		return tipoClienteBC;
	}

	@Override
	protected String getFilterableColumns() {

		return "codigo||nombre";
	}

	@Override
	public String[] getTableColumns() {

		return new String[] { "id", "codigo", "nombre" };
	}

	@RequestMapping("/json")
	@ResponseBody
	public List<TipoCliente> json() {
		return tipoClienteBC.findEntities();
	}

	@RequestMapping("/saludame")
	@ResponseBody
	public String saludar() {
		return "Hola mundo";
	}
}
