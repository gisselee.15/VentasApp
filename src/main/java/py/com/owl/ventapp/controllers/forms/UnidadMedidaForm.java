package py.com.owl.ventapp.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.bc.UnidadMedidaBC;
import py.com.owl.ventapp.controllers.lists.UnidadMedidaList;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.producto.domain.UnidadMedida;

@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@RequestMapping(value = "/unidad-medida")
public class UnidadMedidaForm extends BaseForm<UnidadMedida> {

	@Autowired
	private UnidadMedidaBC unidadMedidaBC;
	@Autowired
	private UnidadMedidaList unidadMedidaListController;


	@Override
	protected UnidadMedida getNewBeanInstance(ModelMap map) {
		UnidadMedida um = new UnidadMedida();
		Empresa empresa = new Empresa(1L);
		um.setEmpresa(empresa);
		return um;
	}
	@Override
	public UnidadMedidaBC getBusinessController() {

		return unidadMedidaBC;
	}

	@Override
	public String getTemplatePath(ModelMap map) {

		return "abm/unidad_medida/unidad_medida_index";
	}

	@Override
	protected UnidadMedidaList getListController() {

		return unidadMedidaListController;
	}
}
