package py.com.owl.ventapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import py.una.cnc.htroot.core.main.UserSession;

@Controller
@Scope("request")
public class Dashboard {

	@Autowired
	private UserSession sesionUsuario;

	@RequestMapping("/")
	public String index() {

		return "redirect:/inicio";
	}

	@RequestMapping("/inicio")
	public String inicio(ModelMap model) {

		model.addAttribute("userSession", sesionUsuario);
		model.addAttribute("currentUser", sesionUsuario.getUser());

		return "index";
	}

}
