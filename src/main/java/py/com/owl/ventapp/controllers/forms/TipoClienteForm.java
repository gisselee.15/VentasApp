package py.com.owl.ventapp.controllers.forms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.ventapp.base.BaseForm;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.controllers.lists.TipoClienteList;
import py.com.owl.ventapp.domain.TipoCliente;

@Controller
@RequestMapping("/tipo-cliente")
@Scope("session")
// Singleton, Session, Request
public class TipoClienteForm extends BaseForm<TipoCliente> {
	@Autowired
	private TipoClienteBC tipoClienteBC;
	@Autowired
	private TipoClienteList tipoClienteList;

	@Override
	public TipoClienteBC getBusinessController() {

		return tipoClienteBC;
	}

	@Override
	protected TipoClienteList getListController() {

		return tipoClienteList;
	}

	@Override
	public String getTemplatePath(ModelMap model) {

		return "/abm/tipo_cliente/tipo_cliente_index";
	}

}
